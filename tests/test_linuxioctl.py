# flake8: noqa

import subprocess

import pytest

import nvme


@pytest.mark.device
def test_linuxioctl():
    serial_paths = subprocess.run(
        ("find", "/sys/devices", "-path", "*/nvme/*/serial"),
        stdout=subprocess.PIPE,
        stderr=subprocess.DEVNULL,
        check=True,
    )
    serials = set()
    for line in serial_paths.stdout.splitlines():
        with open(line) as fh:
            sn = fh.read().strip()
            serials.add(sn)
    assert len(serials) > 0, "device tests must have at least one device present"
    driver = nvme.get_driver("linux")
    devs = driver.scan()
    for dev in devs:
        with dev:
            buffer = driver.new_buffer(4096)
            id_ctrl = driver.id_ctrlr(dev, buffer)
            sn = id_ctrl.sn.decode().strip()
            if sn in serials:
                serials.remove(sn)
            for ns in driver.namespaces(dev):
                with ns:
                    id_ns_buf = driver.new_buffer(4096)
                    id_ns = driver.id_ns(ns, id_ns_buf)
                    lbaf = id_ns.lbafs[id_ns.flbas]
                    lba_size = 2 << lbaf.lbads - 1
                    assert (
                        lba_size <= 4096
                    ), f"this test does not support lbads={lbaf.lbads}"
                    nlb = 4096 // lba_size - 1
                    # If the formatted LBA data size is > 4096, this should read from
                    # unallocated memory... Don't test on a device like that, or fix this.
                    # Also, we're only writing on LBA which is probably 512 bytes. This is
                    # sufficient since it'll write the bytes that contain the serial number.
                    # That's the marker we're using to verify the write/read worked.
                    driver.write(ns, lba=0, nlb=nlb, buffer=buffer).check()
                    read_buf = driver.new_buffer(4096)
                    driver.read(ns, lba=0, nlb=nlb, buffer=read_buf).check()
                    id_ctrl = nvme.IdCtrl.from_buffer(read_buf)
                    assert id_ctrl.sn.decode().strip() == sn, "failed r/w round trip"
    assert len(serials) == 0
