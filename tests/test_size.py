# flake8: noqa

from ctypes import sizeof

from nvme import (
    Cmd,
    CompletionQueueEntry,
    ErrorLogEntry,
    FwSlotLog,
    IdCtrl,
    IdNmsp,
    LbaFormat,
    PowerState,
    SmartLog,
    StatusField,
)


def test_cmd():
    assert sizeof(Cmd) == 64


def test_id_ctrl():
    assert sizeof(PowerState) == 32
    assert sizeof(IdCtrl) == 4096


def test_id_ns():
    assert sizeof(LbaFormat) == 4
    assert sizeof(IdNmsp) == 4096


def test_err_log():
    assert sizeof(ErrorLogEntry) == 64


def test_smart_log():
    assert sizeof(SmartLog) == 512


def test_fw_slot_log():
    assert sizeof(FwSlotLog) == 512


def test_completion():
    assert sizeof(StatusField) == 2
    assert sizeof(CompletionQueueEntry) == 16
